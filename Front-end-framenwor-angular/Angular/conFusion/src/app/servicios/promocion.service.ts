import { Injectable } from '@angular/core';
import {Promocion} from '../carpeta-compartida/promocion';
import {PROMOCIONES} from '../carpeta-compartida/promociones';
import { Observable, of } from 'rxjs'; 
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PromocionService {

  constructor() { }
  getPromociones(): Observable<Promocion[]>{
    return of(PROMOCIONES).pipe(delay(2000));
  
  }

  getPPromocion(id: string): Observable<Promocion>{
    return of(PROMOCIONES.filter((promo) => (promo.id === id))[0]).pipe(delay(2000));
  }


  getFeaturedPromocion(): Observable<Promocion>{
    return of(PROMOCIONES.filter((promo) => promo.featured)[0]).pipe(delay(2000));
  }
}
