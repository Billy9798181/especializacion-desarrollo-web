import { Injectable } from '@angular/core';
import {Plato} from '../carpeta-compartida/plato';
import {PLATOS} from '../carpeta-compartida/platos';
import { Observable, of } from 'rxjs'; 
import { delay } from 'rxjs/operators';
 

@Injectable({
  providedIn: 'root'
})
export class PlatoService {

  constructor() { }

  getPlatos():  Observable<Plato[]> {
    return of (PLATOS).pipe(delay(2000));
  }
 
  getPlato(id: string): Observable<Plato>{
    return of(PLATOS.filter((plato) => (plato.id == id))[0]).pipe(delay(2000));
    }
  


  getFeatureSish(): Observable<Plato>{
    return of(PLATOS.filter((plato) => plato.featured)[0]).pipe(delay(2000)); 
   }

   getPaltoId(): Observable<string[] | any>{
     return of(PLATOS.map(plato => plato.id));
   }

}
