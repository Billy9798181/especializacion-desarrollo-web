import { Component, OnInit, Input,  ViewChild } from '@angular/core';
import {Params, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import {Plato} from '../carpeta-compartida/plato';

import {PlatoService} from '../servicios/plato.service';
import { switchMap} from 'rxjs/operators'

import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Feedback, ContactType} from '../carpeta-compartida/feedback';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {

  
  plato: Plato;
  platoIDs: string[];
  prev: string;
  next: string;

  feedbackForm: FormGroup;
  feedback: Feedback;

  @ViewChild('fform') feedbackFormDirective;

  formErrors = {
    'nombre': '',
    'comentario': '',
    
  };

  formatLabel(value: number) {
    return value;
  }

  validationMessages = {
    
    'nombre': {
      'required': 'primmer nombre es requerido',
      'minlength': 'El nombre debe tener como minimo dos caracteres ',
      'maxlength': 'El nombre no debe esceder de 30 caracteres'
    },
    'comentario': {
      'required': 'comentario es requerido',
      'minlength': 'El apellido debe tener como minimo dos caracteres ',
    }
  };
  
  constructor(private platoService: PlatoService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder) {
      this.createForm();
     }
    
    
    createForm(){
      this.feedbackForm = this.fb.group({
        nombre: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30 )]],
        comentario: ['', [Validators.required, Validators.minLength(2)] ],
      });

      this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

      this.onValueChanged();  
    }

    onValueChanged(data?: any){
      if(!this.feedbackForm) {return; }
      const form = this.feedbackForm;
      for (const field in this.formErrors){
        if(this.formErrors.hasOwnProperty(field)){
          this.formErrors[field] = '';
          const control = form.get(field);
          if( control && control.dirty && !control.valid){
            const messages = this.validationMessages[field];
            for( const key in control.errors){
              if(control.errors.hasOwnProperty(key)){
                this.formErrors[field] += messages[key] + ' ';
              }
            }
            
          }
        }
      }
    }

  ngOnInit() {
    this.platoService.getPaltoId().
      subscribe((platoIDs) => this.platoIDs = platoIDs);
    this.route.params
      .pipe(switchMap((params: Params) => this.platoService.getPlato(params['id'])))
      .subscribe(plato => { this.plato = plato; this.setPrevNext(this.plato.id); });

    
  }

  setPrevNext(platoID: string){
    const index = this.platoIDs.indexOf(platoID);
    this.prev = this.platoIDs[(this.platoIDs.length + index -1) % this.platoIDs.length];
    this.next = this.platoIDs[(this.platoIDs.length + index +1) % this.platoIDs.length];
  }

  goBack(): void{
    this.location.back();
  }

  onSubmit(){
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackForm.reset({
      nombre: '',
      comentario: ''
    });
    this.feedbackFormDirective.resetForm();
  }

}
