import { Component, OnInit } from '@angular/core';
import {Plato} from '../carpeta-compartida/plato';
import {PlatoService} from '../servicios/plato.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  platos: Plato[];

  selectorPlato:Plato;


  

  constructor(private platoService: PlatoService) { 
  }

  ngOnInit() {
    this.platoService.getPlatos()
      .subscribe((platos) => this.platos = platos);
  }

  onSelect(plato: Plato){

    this.selectorPlato = plato;

  }

}
