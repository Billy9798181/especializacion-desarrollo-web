export class  Promocion {

    id: string;
    name: string;
    image: string;
    label: string;
    price: string;
    featured: boolean;
    description: string;

}

