import { Component, OnInit } from '@angular/core';
import {Plato} from '../carpeta-compartida/plato';
import {PlatoService} from '../servicios/plato.service';
import {Promocion} from '../carpeta-compartida/promocion';
import {PromocionService} from '../servicios/promocion.service';
import {Leader} from '../carpeta-compartida/leader';
import {LeaderService} from '../servicios/leader.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  plato : Plato;
  promocion: Promocion;
  leader: Leader;

  constructor(private platoService: PlatoService,
    private proocionService: PromocionService,
    private leaderService: LeaderService) { }

  ngOnInit() {
    this.platoService.getFeatureSish()
      .subscribe(plato => this.plato = plato);
    this.proocionService.getFeaturedPromocion()
      .subscribe(promocion => this.promocion = promocion);
    this.leaderService.getFeatureLeader()
      .subscribe(leader => this.leader = leader);
  }

}
