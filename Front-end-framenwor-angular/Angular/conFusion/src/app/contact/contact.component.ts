import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Feedback, ContactType} from '../carpeta-compartida/feedback';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedback: Feedback;
  contatType = ContactType;

  @ViewChild('fform') feedbackFormDirective;

  formErrors = {
    'firstname': '',
    'lasttname': '',
    'telnum': '',
    'email': ''

  };

  validationMessages = {
    
    'firstname': {
      'required': 'primmer nombre es requerido',
      'minlength': 'El nombre debe tener como minimo dos caracteres ',
      'maxlength': 'El nombre no debe esceder de 30 caracteres'
    },
    'lastname': {
      'required': 'apellido es requerido',
      'minlength': 'El apellido debe tener como minimo dos caracteres ',
      'maxlength': 'El apellido no debe esceder de 30 caracteres'
    },
    'telnum': {
      'required': 'El numero de telefono es requerido',
      'pattern': 'El numero de telefono debe contener solamente numeros.'
    },
    'email':{
      'required': 'El email es requerido',
      'email': 'El correo no contiene el formato valido'
    }
  };

  constructor(private fb: FormBuilder) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm(){
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30 )]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30 )] ],
      telnum: [0, [Validators.required, Validators.pattern]],
      email:['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();  
  }

  onValueChanged(data?: any){
    if(!this.feedbackForm) {return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field] = '';
        const control = form.get(field);
        if( control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          for( const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.formErrors[field] += messages[key] + ' ';
            }
          }
          
        }
      }
    }
  }

  onSubmit(){
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: 0,
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackFormDirective.resetForm();
  }

}
