import { Component, OnInit } from '@angular/core';
import { Leader } from '../carpeta-compartida/leader';
import {LeaderService} from '../servicios/leader.service'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  leaders: Leader[];

  constructor(private leaderService: LeaderService) { }

  ngOnInit() {
     this.leaderService.getLeader()
      .subscribe(leaders => this.leaders = leaders);
  }

}
